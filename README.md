# **Online Chat from React **
***
### JS-библиотеки:
- Redux
- Socket.io
- Axios
- Lodash
- Redux-thunk
- Formik
- React Router
- Classnames
***
### Сервер:
- NodeJS (ExpressJS)
- MongoDB / Mongoose
- Multer
- PassportUS